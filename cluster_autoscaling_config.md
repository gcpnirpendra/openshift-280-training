# Set the auto scale out

## Set the ClusterAutoScaler

- create a `clusterautoscaler.yaml` file
__be sure to set a maximum according to the current capacity__

```yaml
apiVersion: "autoscaling.openshift.io/v1"
kind: "ClusterAutoscaler"
metadata:
  name: "default"
spec:
  podPriorityThreshold: -10 
  resourceLimits:
    maxNodesTotal: 30
    cores:
      min: 24
      max: 240
    memory:
      min: 84 
      max: 840 
  scaleDown: 
    enabled: true 
    delayAfterAdd: 60m 
    delayAfterDelete: 5m 
    delayAfterFailure: 30s 
    unneededTime: 60s 
```

- apply it

```bash
oc apply -f clusterautoscaler.yaml
```

## Set the MachineAutoScaler

- create a `machineautoscaler.yaml` file

```yaml
apiVersion: "autoscaling.openshift.io/v1beta1"
kind: "MachineAutoscaler"
metadata:
  name: "stg-onprem-2tzf2-worker" 
  namespace: "openshift-machine-api"
spec:
   minReplicas: 3
   maxReplicas: 30 
   scaleTargetRef: 
     apiVersion: machine.openshift.io/v1beta1
     kind: MachineSet 
     name: stg-onprem-2tzf2-worker-francecentral1
```

- apply it

```bash
oc apply -f machineautoscaler.yaml
```


## Post configuration tasks

- create a `test-autoscaler-job.yaml` file

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: test
spec:
  parallelism: 100
  completions: 100
  backoffLimit: 4
  template:         
    metadata:
      name: test
    spec:
      containers:
      - name: work 
        image: busybox
        command: ["sleep",  "300"]
      resources:
        requests:
          memory: 500Mi
          cpu: 500m
      restartPolicy: Never 
```

- create it

```bash
oc create -f test-autoscaler-job.yaml
```

- watch the autoscaleout

```bash
oc get machinesets -n openshift-machine-api
```
