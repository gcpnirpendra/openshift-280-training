# Install Openshift 4.7 Cluster with Network Customization

## Introduction

This document describes the installation of OCP4 on Azure.

## Prerequisites

1. Azure Subscription Used - **RND**  [ You will need owner access on this azure subscription ]
   
1. Configure the Azure account.

   1. Configuring a public DNS zone in Azure.

      To install OpenShift Container Platform, the Microsoft Azure account you use must have a dedicated public hosted DNS zone in your account.

      DNS Zone created in azure subscription is **anyname**. 
      [ DNS Zone should be configured in same azure subscription where Installation will be performed ]

      Note - I have used <****> for POC purpose but during actual DR you will use  dns zone.

   1. Create a Service Principal.

      - Log in to the Azure CLI -
      
        ```bash
        az login
        ```

        Log in to Azure in the web console by using your credentials.

      - View your active account details and confirm that the tenantId value matches the subscription you want to use-
   
        ```bash
        az account show
        ```

      - Create the service principal for your account -

        ```bash
        az ad sp create-for-rbac --role Contributor --name <service_principal> 
        ```

          Record the values of the appId and password parameters from the previous output as you will need these values during OpenShift Container Platform installation.
           
      - Grant User Access Administrator permissions to the service principal.

      Note - Use below command to grant user access  admin permissions

      az role assignment create --role "User Access Administrator" --assignee-object-id $(az ad sp list --filter "appId eq '<appId>'" | jq '.[0].objectId' -r)
      

## Installation

   Follow below link with no additional configuration required -

https://docs.openshift.com/container-platform/4.7/installing/installing_azure/installing-azure-network-customizations.html

1. Change to the directory that contains the installation program and run the following command:-

./openshift-install create install-config --dir=<installation_directory> 

2. Change to the directory that contains the installation program and initialize the cluster deployment:-

./openshift-install create cluster --dir=<installation_directory> --log-level=info 

   * Refer to below install-config.yaml during installation -

   install-config.yaml
   
```yaml
apiVersion: v1
baseDomain: <baseDomainName>
compute:
- architecture: amd64
  hyperthreading: Enabled
  name: worker
  platform: {}
  replicas: 3
controlPlane:
  architecture: amd64
  hyperthreading: Enabled
  name: master
  platform: {}
  replicas: 3
metadata:
  creationTimestamp: null
  name: anyname
networking:
  clusterNetwork:
  - cidr: 10.128.0.0/14
    hostPrefix: 23
  machineNetwork:
  - cidr: 10.0.0.0/16
  networkType: OVNKubernetes
  serviceNetwork:
  - 172.30.0.0/16
platform:
  azure:
    baseDomainResourceGroupName: rg-dsi-prd-ocp-dns
    cloudName: AzurePublicCloud
    outboundType: Loadbalancer
    region: francecentral
publish: External
pullSecret: '{"auths":{"cloud.openshift.com":{"auth":"XXXXXX"}}}'
```      

## Registry Installation

There is no additional step required. Storage was pre configured along with installation.

## Challenges Faced

- Creation of Azure subscription took time.
- Getting owner access on Azure subscription took sometime.
- Azure policies applied on subscription did not let creation of Virtual machines initially. We had to get naming convention policy removed from azure subscription.

## External Sources

- https://docs.openshift.com/container-platform/4.6/installing/installing_azure/installing-azure-account.html#installing-azure-account
- https://docs.openshift.com/container-platform/4.6/installing/installing_azure/installing-azure-network-customizations.html
