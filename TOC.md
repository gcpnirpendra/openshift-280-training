## Red Hat OpenShift 4 System Administration
### Day1
### OpenShift Container Platform architecture
 - Overview of Red Hat Enterprise Linux CoreOS (RHCOS)
-  Crio OverView
-  Podman Overview
-  Overview of journactl

### Installation, Upgradation and its troubleshooting
-  Understand the underline infrastructure/resources requirements.
-  Know Quay.io
-  Know Redhat Registry
-  Installation with IPI
-  Installation with customized network plugins
-  Troubleshooting installation issues
-  Gathering logs from a failed installation
-  Manually gathering logs with SSH access to your host(s)
-  Manually gathering logs without SSH access to your host(s)
-  Getting debug information from the installation program
-  Login to master nodes and worker nodes

### Day 2
### Post_installation_configuration and Troubleshooting
 - Configuration of Authentication with Htpasswd
-  Configuration of Authentication with Azure AD (Optional)
-  Remove the default virtual admin user (kubeadmin)
-  Secure Api with ssl certificate
-  Secure Route with Route
-  Setting the Ingress Controller
-  Configuration Default Quota project template
-  Configure default limits
-  Restrict user for LoadBalancer service.
-  Configure Alert Manager
-  Updating the global cluster pull secret
-  Configure Autoscaling for nodes
-  Exposing OpenShift Internal Registry
-  Login to Internal Registry ,pull and push images
-  Monitoring pods

### Day 3
### OpenShift Backup and DR (Optional)
 - Installation and Configuration of Kasten/Velero
-  Setup the backup of etcd
-  Recovering from the etcd backup

### Post-installation node tasks
- Configuring Machine health checks
-  Node host best practices
-  Configure different type of profile
-  Updating ssh keys for master and worker nodes

### Post-installation storage configuration
- Dynamic provisioning
-  Defining a storage class

### OpenShift Scc and troubleshooting
 - Understanding default scc
-  Creating and user custom scc
-  Ubable to deploy application because of scc
-  Adding a service account to deploy an application

### Pod Scheduling and Troubleshooting
 - Default scheduling
-  Infrastructure Topological Levels
-  Affinity
-  Anti Affinity
-  Advanced scheduling
-  Pod Affinity and Anti-affinity
-  Node Affinity
-  Node Selectors
-  Taints and Tolerations

### Day 4

### Managing Sensitive Information with Secrets
-  Guided Exercise: Managing Sensitive Information With Secrets
-  Controlling Application Permissions with Security Context Constraints (SCCs)
-  Guided Exercise: Controlling Application Permissions with Security Context

### Configuring OpenShift Networking Components
 - Controlling Cluster Network Ingress
-  Guided Exercise: Controlling Cluster Network Ingress
-  Lab: Configuring OpenShift Networking Components

### Scaling an Application
 - Scaling an Application
-  Controlling Pod Scheduling
-  Scaling an OpenShift Cluster
-  Manually Scaling an OpenShift Cluster
-  Guided Exercise: Manually Scaling an OpenShift Cluster
-  Automatically Scaling an OpenShift Cluster

### Managing a Cluster with the Web Console
 - Performing Cluster Administration
-  Guided Exercise: Performing Cluster Administration
-  Managing Workloads
-  Guided Exercise: Managing Workloads
-  Examining Cluster Metrics
-  Guided Exercise: Examining Cluster Metrics
-  Lab: Managing the Cluster with the Web Console
-  Lab: Install, manage, and troubleshoot an OpenShift cluster
-  Troubleshooting

### Day 5
 - Pod related issues
-  Router/Registry Not deploying to correct node
-  Failure to deploy registry (permissions issues)
-  Application Pod fails to deploy
-  Issues with Nodes
-  Nodes being reported as ready, but builds failing
-  Node reporting NotReady
-  Nodes report ready but ETCD health check fails
- 
-  Atomic-openshift-node service fails to start
-  Must make a non-zero request for cpu
-  Issues related to Identity
-  user is unable to login
-  user has two identities
-  How to impersonate user
-  login with service account

