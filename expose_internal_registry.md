# Expose internal registry

```bash
oc patch configs.imageregistry.operator.openshift.io/cluster --patch '{"spec":{"defaultRoute":true}}' --type=merge
```

# Post configuration tests
- From your local machine, try to docker push a container image in your own namespace
```
docker login -u x -p $(oc whoami -t) 
docker tag alpine:latest url/test-image/alpine:latest
docker push default-route-openshift-image-registry.apps.stg-onprem..ga/test-image/alpine:latest
```

# Expected Result


b2d5eeeaba3a: Pushed
latest: digest: sha256:def822f9851ca422481ec6fee59a9966f12b351c62ccb9aca841526ffaa9f748 size: 528

```
oc get istag -n test-image
NAME            IMAGE REFERENCE                                                                                                                              UPDATED
alpine:latest   image-registry.openshift-image-registry.svc:5000/test-image/alpine@sha256:def822f9851ca422481ec6fee59a9966f12b351c62ccb9aca841526ffaa9f748   11 seconds ago
```
